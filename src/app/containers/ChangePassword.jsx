import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../actions/auth.js';
import { putUser } from '../actions/users.js';
import ChangePassword from '../components/ChangePassword.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    changeUserPassword: (token, user) => {
        dispatch(putUser(token, user));
    },    
    onLogout: (username) => {
        dispatch(logout(username));
    }
  }
}
const mapStateToProps = (state) => ({
    'credential': state.credential,
    'users': state.users
});

const ChangePwd = connect(mapStateToProps, mapDispatchToProps)(ChangePassword)
export default ChangePwd


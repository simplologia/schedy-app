import React from 'react';
import { connect } from 'react-redux';
import { postClient } from '../actions/clients.js';
import { changeMainTitle } from '../actions/schedy.js';
import CreateClient from '../components/clients/Create.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    postClient: (token, client) => {
        dispatch(postClient(token, client));
    },
    changeMainTitle: (title) => {
        dispatch(changeMainTitle(title));
    }
  }
}

const mapStateToProps = (state) => ({
    'credential': state.credential,
    'client': state.client
});

const ManageClients = connect(mapStateToProps, mapDispatchToProps)(CreateClient)
export default ManageClients


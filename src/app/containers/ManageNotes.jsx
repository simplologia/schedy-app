import React from 'react';
import { connect } from 'react-redux';
import { postNote, getNotesByService } from '../actions/note.js';
import CreateNotes from '../components/notes/Create.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    postNote: (token, note) => {
        dispatch(postNote(token, note));
    },
    getNotesByService: (service, token) => {
        dispatch(getNotesByService(service, token));
    }
  }
}

const mapStateToProps = (state) => ({
    'credential': state.credential,
    'service': state.service,
    'note': state.note
});

const ManageNote = connect(mapStateToProps, mapDispatchToProps)(CreateNotes)
export default ManageNote



import React from 'react';
import { connect } from 'react-redux';
import { authenticateMe } from '../actions/auth.js';
import Login from '../components/Login.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    authenticateMe: (username, password) => {
        dispatch(authenticateMe(username, password));
    },
  }
}

const mapStateToProps = (state) => ({
  'credential': state.credential,
  'users': state.users
});

const Authenticate = connect(mapStateToProps, mapDispatchToProps)(Login)
export default Authenticate


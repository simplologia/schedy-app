import { changeMainTitle } from './schedy.js';
import fetch from 'isomorphic-fetch';

export const createClient = (client) => ({
  type: 'CREATE_CLIENT',
  id: client.id,
  name: '',
  email: '',
  cpf: '',
  birthDate: '',
  created: true
});

export const failCreateClient = (client) => ({
  type: 'FAIL_CREATE_CLIENT',
  id: 0,
  name: '',
  email: '',
  cpf: '',
  birthDate: '',
  created: false
});

export const listClients = (clients) => ({
  type: 'LIST_CLIENT',
  items: clients,
  created: null
});

export function postClient(token, client) {
  var formData = new FormData();

  formData.append('name', client.name);
  formData.append('e_mail', client.email);
  formData.append('cpf', client.cpf);
  formData.append('birth_date', client.birthDate);
  formData.append('alias', client.alias ? client.alias : 'nenhum');
  formData.append('cnpj', client.cnpj ? client.cnpj : 'nenhum');
  formData.append('address', client.address);
  formData.append('phone', client.phone);

  return dispatch => {
    return fetch('/api/clients/', {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 201) {
        return response.json();
      } else {
        dispatch(failCreateClient(client));
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(createClient(json));
    })
  };
}

export function getClients(token) {
  return dispatch => {
    return fetch('/api/clients/', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }
    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
    }).then(function(json) {
      if (!json) return;
      dispatch(listClients(json.results));
    })
  };
}

import React from 'react';
import { Redirect } from 'react-router-dom'
import { MDCTextfield, MDCTextfieldFoundation } from '@material/textfield';
import SnackBar from '../SnackBar.jsx';

class CreateClient extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      'name': '',
      'birthDate': '',
      'address': '',
      'phone': '',
      'email': '',
      'cpf': ''
    }

    let credential = this.props.credential;

    if (credential && credential.authenticated){
      this.token = credential.token;
    }
    this.onChange = this.onChange.bind(this);
    this.onCreate = this.onCreate.bind(this);
    this.postClient = props.postClient;
    this.changeMainTitle = props.changeMainTitle;
    this.setupMainToolbarActionButtons = props.setupMainToolbarActionButtons;
    this.isSnackBarEnabled = false;
  }

  onCreate(event){
    this.isSnackBarEnabled = true;
    this.postClient(this.token, {
        'name': this.state.name,
        'email': this.state.email,
        'cpf': this.state.cpf,
        'birthDate': this.state.birthDate
      }
    );
    event.preventDefault();
  }

  onChange(event){
    this.setState({ [event.target.name]: event.target.value});
  }

  render() {
    if (!this.props.credential.authenticated){
      return <Redirect to={{pathname: '/login'}}/>
    }

    var errorMessage = null;
    if (this.props.client && this.props.client.hasOwnProperty('created') &&
        !this.props.client.created){
      errorMessage = "Não foi possível criar o cliente.";
    }

    if (this.props.client && this.props.client.hasOwnProperty('created') &&
        this.props.client.created){
      errorMessage = "Cliente criado com sucesso.";
      if (this.showSnackbar) {
        this.setState({
          'name': '',
          'birthDate': '',
          'address': '',
          'phone': '',
          'email': '',
          'cpf': ''});
      }
    }

    var component = (
      <div className="content-body">
        <div className="clients-grid mdc-layout-grid mdc-elevation--z4">
          <form onSubmit={this.onCreate}>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-8">
                <div className='mdc-textfield form-field name'>
                  <input id='id_name' type='text' className='mdc-textfield__input' name='name' onChange={this.onChange} value={this.state.name}/>
                  <label className='mdc-textfield__label' htmlFor='name'>Nome</label>
                </div>
                </div>
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                <div className='mdc-textfield form-field birthDate'>
                  <input id='id_birthDate' type='text' className='mdc-textfield__input' name='birthDate' onChange={this.onChange} value={this.state.birthDate}/>
                  <label className='mdc-textfield__label' htmlFor='birthDate'>Data de Nascimento</label>
                </div>
              </div>
            </div>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-8">
                <div className='mdc-textfield form-field address'>
                  <input id='id_address' type='text' className='mdc-textfield__input' name='address' onChange={this.onChange} value={this.state.address}/>
                  <label className='mdc-textfield__label' htmlFor='address'>Endereço completo</label>
                </div>
              </div>
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                <div className='mdc-textfield form-field phone'>
                  <input id='id_phone' type='text' className='mdc-textfield__input' name='phone' onChange={this.onChange} value={this.state.phone}/>
                  <label className='mdc-textfield__label' htmlFor='phone'>Telefone</label>
                </div>
              </div>
            </div>
            <div className="mdc-layout-grid__inner">
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-8">
                <div className='mdc-textfield form-field email'>
                  <input id='id_email' type='text' className='mdc-textfield__input' name='email' onChange={this.onChange} value={this.state.email}/>
                  <label className='mdc-textfield__label' htmlFor='email'>Email</label>
                </div>
              </div>
              <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                <div className='mdc-textfield form-field cpf'>
                  <input id='id_cpf' type='text' className='mdc-textfield__input' name='cpf' onChange={this.onChange} value={this.state.cpf}/>
                  <label className='mdc-textfield__label' htmlFor='cpf'>CPF</label>
                </div>
              </div>
            </div>
            <br/>
            <br/>
            <button className='mdc-button mdc-button--raised mdc-button--dense' type='submit'>Cadastrar</button>
            <SnackBar isSnackBarEnabled={this.isSnackBarEnabled} message={errorMessage}/>
          </form>
        </div>
      </div>
    );
    this.isSnackBarEnabled = false;
    return component;
  }

  componentDidMount() {
    MDCTextfield.attachTo(document.querySelector('.name'));
    MDCTextfield.attachTo(document.querySelector('.birthDate'));
    MDCTextfield.attachTo(document.querySelector('.address'));
    MDCTextfield.attachTo(document.querySelector('.phone'));
    MDCTextfield.attachTo(document.querySelector('.email'));
    MDCTextfield.attachTo(document.querySelector('.cpf'));
    this.changeMainTitle('Cadastrar Clientes');
    this.setupMainToolbarActionButtons(false);
  }
}

export default CreateClient;
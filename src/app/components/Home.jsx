import React from 'react'
import { Redirect } from 'react-router-dom'
import ServiceList from './services/List.jsx'

class Home extends React.Component {

  constructor(props){
    super(props);
    this.postService = props.postService;
    this.changeMainTitle = props.changeMainTitle;
    this.setupMainToolbarActionButtons = props.setupMainToolbarActionButtons;
    let credential = props.credential;
    if (credential.authenticated){
      this.props.getServicesByProfessional(credential.token, credential.id);
    }      
    
  }

  render() {
    let credential = this.props.credential;
    if (!credential.authenticated){
      return <Redirect to={{pathname: '/login'}}/>
    }      


    return (
      <div className="content-body">
        <ServiceList service={this.props.servicesByProfessional}/>
      </div>
    )
  }

  componentDidMount() {
    this.changeMainTitle('Meus Serviços');
    this.setupMainToolbarActionButtons(false);
  }
}

export default Home;
